package com.example.ramon.fidelis;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class Login extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar          = (Toolbar) findViewById(R.id.toolbar);
        Button botaoLogin        = (Button) findViewById(R.id.buttonAcessar);
        Button botaoCadastrar    = (Button) findViewById(R.id.buttonCadastrar);
        Button botaoEsqueciSenha = (Button) findViewById(R.id.buttonEsqueciSenha);
        setSupportActionBar(toolbar);

        botaoLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent abrirMenuPrincipal = new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(abrirMenuPrincipal);
            }
        });

        botaoCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent abrirTelaCadastro = new Intent(getApplicationContext(), Perfil.class);
                startActivity(abrirTelaCadastro);
            }
        });



    }

}

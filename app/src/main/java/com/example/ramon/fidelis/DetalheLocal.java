package com.example.ramon.fidelis;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class DetalheLocal extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe_local);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        Button botaoResgatar = (Button) findViewById(R.id.buttonResgatar);
        setSupportActionBar(toolbar);

        botaoResgatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent abrirResgate = new Intent(getApplicationContext(), Resgate.class);
                startActivity(abrirResgate);
            }
        });
    }

}

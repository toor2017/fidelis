package com.example.ramon.fidelis.modelo;

import java.util.List;

/**
 * Created by Ramon on 27/06/2017.
 */

public class Empresa {
    private long id;
    private String Empresa;
    private String dataUltimaCompra;
    private List<String> Promocao;
    private int quantidadeCompras;

    public Empresa(String empresa, String dataUltimaCompra, List<String> promocao, int quantidadeCompras) {
        this.Empresa = empresa;
        this.dataUltimaCompra = dataUltimaCompra;
        this.Promocao = promocao;
        this.quantidadeCompras = quantidadeCompras;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmpresa() {
        return Empresa;
    }

    public void setEmpresa(String empresa) {
        Empresa = empresa;
    }

    public String getDataUltimaCompra() {
        return dataUltimaCompra;
    }

    public void setDataUltimaCompra(String dataUltimaCompra) {
        this.dataUltimaCompra = dataUltimaCompra;
    }

    public List<String> getPromocao() {
        return Promocao;
    }

    public void setPromocao(List<String> promocao) {
        Promocao = promocao;
    }

    public int getQuantidadeCompras() {
        return quantidadeCompras;
    }

    public void setQuantidadeCompras(int quantidadeCompras) {
        this.quantidadeCompras = quantidadeCompras;
    }

    @Override
    public String toString() {
        return this.Empresa;
    }
}

package com.example.ramon.fidelis;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.ramon.fidelis.adapter.EmpresaAdapter;
import com.example.ramon.fidelis.modelo.Empresa;

import java.util.Arrays;
import java.util.List;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private ListView listaEmpresas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        listaEmpresas = (ListView) findViewById(R.id.lista_Empresa);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        listaEmpresas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                //Empresa empresa = (Empresa) listaEmpresas.getItemAtPosition(position);

                Intent intentVaiPraEmpresa = new Intent(HomeActivity.this, DetalheLocal.class);
                startActivity(intentVaiPraEmpresa);
            }
        });
        carregaLista();
    }

    private void carregaLista() {

        List<String>  promocoesPetrobras = Arrays.asList("Abasteça e ganhe um lava rápido", "Compre e leve um prêmio");
        Empresa empresaPetrobras = new Empresa("Petrobras", "25/05/2016", promocoesPetrobras, 8);

        List<String> PromocoesPadaria = Arrays.asList("Compre 5 leve 10", "Compre 4 ganhe lanche");
        Empresa empresaPadaria = new Empresa("Padaria", "10/06/2017", PromocoesPadaria , 4);

        List<Empresa> empresas = Arrays.asList(empresaPadaria, empresaPetrobras);

        EmpresaAdapter adapter = new EmpresaAdapter(this, empresas);
        listaEmpresas.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //@SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_perfil)
        {
            Intent abrirPerfil = new Intent(HomeActivity.this, Perfil.class);
            startActivity(abrirPerfil);
        }
        else if (id == R.id.nav_locaisvisitados)
        {
            Intent abrirLocaisVisitados = new Intent(this, LocaisVisitados.class);
            startActivity(abrirLocaisVisitados);
        }
        else if (id == R.id.nav_historico)
        {
            Intent abrirHistorico = new Intent(HomeActivity.this, Historico.class);
            startActivity(abrirHistorico);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}

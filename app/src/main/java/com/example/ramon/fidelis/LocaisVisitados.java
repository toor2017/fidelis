package com.example.ramon.fidelis;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import static com.example.ramon.fidelis.R.layout.activity_locais_visitados;

public class LocaisVisitados extends AppCompatActivity {

    private Toolbar toolbar;
    private CardView card1;
    private CardView card3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locais_visitados);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        card1 = (CardView) findViewById(R.id.cardLocal1);
        card3 = (CardView) findViewById(R.id.cardLocal3);


        setSupportActionBar(toolbar);

        card1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent abrirLocalVisitado1 = new Intent(getApplicationContext(), DetalheLocal.class);
                startActivity(abrirLocalVisitado1);
            }
        });
        card3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent abrirLocalVisitado2 = new Intent(getApplicationContext(), DetalheLocal.class);
                startActivity(abrirLocalVisitado2);
            }
        });
    }

}

package com.example.ramon.fidelis.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.ramon.fidelis.HomeActivity;
import com.example.ramon.fidelis.R;
import com.example.ramon.fidelis.modelo.Empresa;

import java.util.List;

/**
 * Created by Ramon on 27/06/2017.
 */

public class EmpresaAdapter extends BaseAdapter{

    private final Context context;
    private final List<Empresa> empresas;

    public EmpresaAdapter(Context context, List<Empresa> empresa) {
        this.context = context;
        this.empresas = empresa;
    }

    @Override
    public int getCount() {
        return empresas.size();
    }

    @Override
    public Object getItem(int position) {
        return empresas;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Empresa empresa = empresas.get(position);
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = convertView;
        view = inflater.inflate(R.layout.conteudo_home, parent, false);

        TextView textEmpresa = (TextView) view.findViewById(R.id.textEmpresa);
        if(textEmpresa != null) {
            textEmpresa.setText(empresa.getEmpresa());
        }

        TextView textPromocao = (TextView) view.findViewById(R.id.textViewPromocao);
        if(textPromocao != null) {
            textPromocao.setText("Promocao Teste");
        }

        TextView textQtdCompras = (TextView) view.findViewById(R.id.textQtdCompras);
        if(textQtdCompras != null) {
            textQtdCompras.setText(String.valueOf(empresa.getQuantidadeCompras()));
        }

        TextView textUltimaCompra = (TextView) view.findViewById(R.id.textUltimaCompra);
        if(textUltimaCompra != null) {
            textUltimaCompra.setText(empresa.getDataUltimaCompra());
        }

        return view;
    }
}
